package osom.tanks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl.LwjglFiles;
import com.badlogic.gdx.backends.lwjgl.LwjglNativesLoader;
import com.esotericsoftware.minlog.Log;
import osom.tanks.server.ServerApplicationListener;
import osom.tanks.server.ServerConfig;
import osom.tanks.sim.Configuration;

public class ServerMain {
	public static void main(String[] args) {


        ServerConfig serverConfig = new ServerConfig();
        for(String arg : args) {

            if(arg.equals("-lag"))
                serverConfig.simulateLag = true;

            if(arg.equals("-headless"))
                serverConfig.headless = true;
        }

        if(args.length > 1) {
            serverConfig.simulateLag = args[1].equals("-lag");
        }

        if(serverConfig.headless) {
            LwjglNativesLoader.load();
            Gdx.files = new LwjglFiles();
            ServerApplicationListener tanks = new ServerApplicationListener(serverConfig);
            tanks.create();

            long targetFrameTime = Configuration.SIM_FRAME_TIME;
            long sleepTime, frameStartTime, frameEndTime, frameLength;
            long frameCnt = 0;
            long lagSum = 0;

            long lastFrame = 0;

            while (true) {

                frameStartTime = System.currentTimeMillis();
                tanks.render();

//                Log.error("Frame time: " + (System.currentTimeMillis() - lastFrame));
                lastFrame = System.currentTimeMillis();

                frameEndTime = System.currentTimeMillis();

                frameLength = frameEndTime - frameStartTime;
                sleepTime = targetFrameTime - frameLength;

                if(sleepTime < 0) {
                    lagSum += -sleepTime;
                    sleepTime = 0;
                    Log.error("Server is lagging behind, frame " + frameCnt + " took: " + frameLength + " lag sum: " + lagSum);
                }

                frameCnt += 1;

                try {
                    if(sleepTime > 0 && lagSum > 0) {
                        lagSum -= sleepTime;
                        sleepTime = 0;
                        if(lagSum < 0) {
                            sleepTime = -lagSum;
                            lagSum = 0;
                        }
                    }
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        } else {
            LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
            cfg.title = "tanks";
            cfg.useGL20 = false;
            cfg.width = 480;
            cfg.height = 320;
            cfg.backgroundFPS = (int)Configuration.SIM_FRAME_TIME;
            cfg.foregroundFPS = (int)Configuration.SIM_FRAME_TIME;
            new LwjglApplication(new ServerApplicationListener(serverConfig), cfg);
        }
	}
}
