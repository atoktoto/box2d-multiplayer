package osom.tanks;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import osom.tanks.client.ClientApplicationListener;
import osom.tanks.client.ClientConfig;

public class ClientMain {
	public static void main(String[] args) {

		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "tanks";
		cfg.useGL20 = false;
		cfg.width = 480;
		cfg.height = 320;
        cfg.foregroundFPS = 90;
        cfg.vSyncEnabled = false;
        cfg.backgroundFPS = 90;

        if(args.length == 0) {
            System.out.println("Usage: client.jar [address]");
            return;
        }

        ClientConfig clientConfig = new ClientConfig();
        clientConfig.address = args[0];



        if(args.length > 1) {
            clientConfig.simulateLag = args[1].equals("-lag");
        }

		new LwjglApplication(new ClientApplicationListener(clientConfig), cfg);
	}
}
