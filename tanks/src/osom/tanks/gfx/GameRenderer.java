package osom.tanks.gfx;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import osom.tanks.sim.GameWorld;
import osom.tanks.client.GameInputListener;

public class GameRenderer implements ApplicationListener {

    private Stage stage;
    private OrthographicCamera camera;
    private Box2DDebugRenderer debugRenderer;

    private GameInputListener gameInputListener = null;
    private World world;
    private GameWorld gameWorld;

    private BitmapFont titleFont;
    private BitmapFont textFont;

    public GameRenderer(GameWorld gameWorld, World boxWorld) {
        world = boxWorld;
        this.gameWorld = gameWorld;
    }

    @Override
    public void create() {

        //TODO extract
        FileHandle fontFile = Gdx.files.internal("data/Roboto-Bold.ttf");
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);
        titleFont = generator.generateFont(30);
        textFont = generator.generateFont(20);
        generator.dispose();

        camera = new OrthographicCamera();
        stage = new Stage();
        stage.setCamera(camera);
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Gdx.input.setInputProcessor(stage);

        debugRenderer = new Box2DDebugRenderer(true, true, false, true, true, true);
    }

    @Override
    public void resize(int width, int height) {
        stage.setCamera(camera);
        stage.setViewport(40, 40 * (height/(float)width));
        //TODO zoom position
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        if(Math.abs(camera.zoom - requestedZoom) > 0.001) {
            float delta = requestedZoom - camera.zoom;
            zoomAtCursor(delta/2);
        }

        debugRenderer.render(world, camera.combined);
        stage.draw();

        stage.getSpriteBatch().begin();
        titleFont.setScale(0.05f);
        titleFont.draw(stage.getSpriteBatch(), "" + gameWorld.frameCnt, 0.2f, 1.2f);
        titleFont.setScale(0.04f);
        titleFont.draw(stage.getSpriteBatch(), "" + (int)(1f / Gdx.graphics.getRawDeltaTime()), 8f, 1.2f);
        stage.getSpriteBatch().end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }


    public float requestedZoom = 1;
    public void requestZoom(float delta) {
        requestedZoom += delta;
        if(requestedZoom < 0.1)
            requestedZoom = 0.1f;
        Gdx.app.log("zoom", "" + requestedZoom);
    }

    public void zoomAtCursor(float amount) {
        float oldZoom = camera.zoom;
        camera.zoom += amount;

        //TODO zoom at cursor
//        float a = (Gdx.input.getX() - (float)Gdx.graphics.getWidth() / 2) * (oldZoom - camera.zoom);
//        float b = (-Gdx.input.getY() + (float)Gdx.graphics.getHeight() / 2) * (oldZoom - camera.zoom);
//        camera.translate(a, b);
    }

    public void setInputListener(GameInputListener inputListener) {
        gameInputListener = inputListener;
        stage.addListener(gameInputListener);
    }

    public void cameraTranslate(float x, float y) {
        stage.getCamera().translate(x, y, 0);
    }
}
