package osom.tanks.messages;

public class EngineStateChange extends GameMessage {
    public boolean on = false;
    public int engineGroup = -1;
}
