package osom.tanks.sim;


public class Configuration {
    public static final long SIM_FRAME_TIME = 16;
    public static final int MAX_BLOCKS = 1000;
}
