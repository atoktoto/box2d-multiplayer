package osom.tanks.sim;

import com.esotericsoftware.minlog.Log;
import osom.tanks.entities.BaseBlock;
import osom.tanks.entities.EngineBlock;
import osom.tanks.entities.Player;
import osom.tanks.messages.GameMessage;
import osom.tanks.messages.WorldUpdate;
import osom.tanks.networking.ServerNetworking;

import java.util.ArrayList;

/**
 * Server version of SimulationRunner. Added functionality includes a lag compensation.
 */
public class ServerSimulationRunner extends SimulationRunner {

    private ServerNetworking server;
    private ArrayList<Player> playersInGame = new ArrayList<Player>();

    public ServerSimulationRunner(GameWorld gameWorld, ServerNetworking serverNetworking) {
        super(gameWorld);
        this.server = serverNetworking;
    }

    public void executeMessage(GameMessage message, Player who) {
        long messageAge = gameWorld.frameCnt - message.frameCnt;
        Log.error("Message age:" + messageAge);

        if(messageAge >= STATE_MEMORY_LEN) {
            Log.error("Discarded super-old message");
            //TODO run it as early as we can?
            sendWorldUpdate(true);
        } else if(messageAge < 0) {

            Log.error("Server lagging behind");

            // client is ahead of us. Execute as usual.
            gameWorld.executeMessage(message, who);
            updatePhysics();
            sendWorldUpdate(true);  //TODO smaller update scope ?

        } else if(messageAge > 0) {
            goBackToFrame(message.frameCnt);
            gameWorld.executeMessage(message, who);
            updatePhysics();
            advanceSimulation(messageAge);
            sendWorldUpdate(false);
        } else if(messageAge == 0) {
            gameWorld.executeMessage(message, who);
            updatePhysics();
            sendWorldUpdate(false);
        }
    }

    private void negotiatePlayer(Player player) {

        boolean noChanges = true;

        while (true) {
            for(Player p : playersInGame) {
                if(player.nickname.equals(p.nickname)) {
                    player.nickname = player.nickname + "*";
                    noChanges = false;
                }

                if(player.id == p.id) {
                    player.id += 1;
                    noChanges = false;
                }
            }
            if(noChanges) break;
        }
    }

    public Player userJoined(Player player) {
        negotiatePlayer(player);
        playersInGame.add(player);

        EngineBlock engine1 = new EngineBlock();  //TODO not really a right place for that
        engine1.positionX = 20;
        engine1.positionY = 22.2f;
        engine1.engineGroup = 1;
        engine1.owner = player.id;
        engine1.setThrustAngle(270);
        gameWorld.addBlock(engine1);
        createBody(engine1);

        EngineBlock engine2 = new EngineBlock();  //TODO not really a right place for that
        engine2.positionX = 22.2f;
        engine2.positionY = 20;
        engine2.engineGroup = 2;
        engine2.owner = player.id;
        engine2.setThrustAngle(270);
        gameWorld.addBlock(engine2);
        createBody(engine2);

        EngineBlock engine3 = new EngineBlock();  //TODO not really a right place for that
        engine3.positionX = -2.2f;
        engine3.positionY = 20f;
        engine3.engineGroup = 4;
        engine3.owner = player.id;
        engine3.setThrustAngle(270);
        gameWorld.addBlock(engine3);
        createBody(engine3);

        EngineBlock engine4 = new EngineBlock();  //TODO not really a right place for that
        engine4.positionX = 20;
        engine4.positionY = -2.2f;
        engine4.engineGroup = 3;
        engine4.owner = player.id;
        engine4.setThrustAngle(90);
        gameWorld.addBlock(engine4);
        createBody(engine4);

        BaseBlock baseBlock = new BaseBlock();
        baseBlock.positionX = 20;
        baseBlock.positionY = 20f;
        baseBlock.links[0] = engine1.id;
        baseBlock.links[1] = engine2.id;
        baseBlock.links[2] = engine3.id;
        baseBlock.links[3] = engine4.id;
        gameWorld.addBlock(baseBlock);
        createBody(baseBlock);

        updatePhysics();
        sendWorldUpdate(true);

        return player;
    }

    public void userQuited() {

    }

    // TODO move out
    public void sendWorldUpdate(boolean syncFrameCnt) {
        WorldUpdate worldUpdate = new WorldUpdate();
        worldUpdate.gameWorld = getWorldSnapshot();
        worldUpdate.syncFrame = syncFrameCnt;
        server.sendWorldUpdate(worldUpdate);
    }


}
