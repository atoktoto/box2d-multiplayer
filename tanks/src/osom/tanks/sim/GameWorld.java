package osom.tanks.sim;

import osom.tanks.entities.BaseBlock;
import osom.tanks.entities.EngineBlock;
import osom.tanks.entities.Player;
import osom.tanks.messages.EngineStateChange;
import osom.tanks.messages.GameMessage;
import osom.tanks.messages.TestClickAction;

/**
 * Container with the whole state of the game world. Need to be synchronized with Box2D entities.
 * This object is being sent as a part of GameWorldUpdate when server requests clients to update.
 */
public class GameWorld {

    /**
     * Simulation time.
     */
    public long frameCnt = 0;

    public BaseBlock[] blocks = new BaseBlock[1000];
    public int blockIdCnt = 0;

    /**
     * Adds provided block to the world index. Assigns ID.
     * @param block object being decorated with the ID.
     * @return same object for chaining.
     */
    public BaseBlock addBlock(BaseBlock block) {
        block.id = blockIdCnt;
        blockIdCnt++;
        blocks[block.id] = block;
        return block;
    }

    public void executeMessage(GameMessage message, Player who) {
        if(message instanceof TestClickAction) {
            testClick(((TestClickAction) message).x, ((TestClickAction) message).y, who);
        } else if (message instanceof EngineStateChange) {
            changeEngineState((EngineStateChange) message, who);
        }
    }

    public void testClick(float x, float y, Player who) {
//        BaseBlock testTank = blocks[who.id];
//        testTank.setPosition(x, y);
//        testTank.linearVelocityX = 0;
//        testTank.linearVelocityY = 0;
    }

    public void changeEngineState(EngineStateChange change, Player who) {
        for(BaseBlock block : blocks) {
            if(block != null && block instanceof EngineBlock) {
                EngineBlock engine = (EngineBlock)block;
                if(engine.owner == who.id && engine.engineGroup == change.engineGroup) {
                    engine.setThrustOn(change.on);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "GameWorld @ f:" + frameCnt;
    }

    public void createNew() {

    }
}
