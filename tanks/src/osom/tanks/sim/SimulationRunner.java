package osom.tanks.sim;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.kryo.Kryo;
import osom.tanks.entities.BaseBlock;

import java.util.LinkedList;

/**
 * Manages synchronization between Box2D world and objects used for communication and high level access.
 */
public class SimulationRunner {

    /**
     * Number of frames remembered in order to allow making changes in server history.
     */
    protected final int STATE_MEMORY_LEN = 60;

    /**
     * Structure containing number of past simulation states. When a user action arrives to the server,
     * server goes back in time and updates the simulation results as if the action was acknowledged
     * by the server in the same frame it was input by the user. LinkedList is used as a circular buffer.
     */
    protected transient LinkedList<GameWorld> stateMemory = new LinkedList<GameWorld>();

    protected final GameWorld gameWorld;
    protected final World boxWorld;

    /**
     * Used for making a deep copy of the game state.
     */
    private final Kryo kryo = new Kryo();

    private BodyFinder finder = new BodyFinder();


    public SimulationRunner(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        this.boxWorld = new World(new Vector2(0, 0), true);
    }

    protected Body createBody(BaseBlock block) {
        if(block.id == -1) {
            throw new RuntimeException("Set BaseBlock.id before creating body");
        }

        return finder.setBody(block, BaseBlock.createBlockBody(boxWorld));
    }



    protected void goBackToFrame(long frameNumber) {
        long lastFrameOnStack = stateMemory.peekFirst().frameCnt;
        int backNumber = (int)(frameNumber - lastFrameOnStack);
        GameWorld oldWorld = stateMemory.get(backNumber);

        //clear history (in will be rebuild after applying messages)
        long backNumber2 = stateMemory.peekLast().frameCnt - frameNumber;
        for(int i=0; i<=backNumber2; i++) {
            stateMemory.pollLast();
        }

        gameWorld.frameCnt = oldWorld.frameCnt;
        setWorldState(oldWorld);
    }

    public void updatePhysics() {
        //TODO remove not used bodies

        for(BaseBlock block : gameWorld.blocks) {
            if(block != null) {
                finder.setBody(block, block.updateBody(finder.getBody(block), boxWorld, gameWorld));
                //TODO setbody?
            }
        }

        for(BaseBlock block : gameWorld.blocks) {
            if(block != null) {
                block.updateJoints(finder.getBody(block), boxWorld, gameWorld, finder);
            }
        }
    }

    public void updateObjects() {
        for(BaseBlock block : gameWorld.blocks) {
            if(block != null)
                block.updateObject(finder.getBody(block));
        }
    }

    protected void advanceSimulation(long frames) {
        for(int i = 0; i < frames; i++) {
            tick();
        }
    }

    public void tick() {
        stateMemory.addLast(getWorldSnapshot());
        if(stateMemory.size() > STATE_MEMORY_LEN) {
            stateMemory.pollFirst();
        }

        gameWorld.frameCnt++;
        boxWorld.step(1/60f, 6, 2);

        for(BaseBlock block : gameWorld.blocks) {
            if(block != null)
                block.tick(finder.getBody(block));
        }

        updateObjects();
    }

    protected void setWorldState(GameWorld fromWorld) {
        // frame number
        gameWorld.frameCnt = fromWorld.frameCnt;

        // blocks state FIXME ??
        for(int i = 0; i < fromWorld.blocks.length; i++) {
//            TankBlock ours = gameWorld.blocks[i];
            gameWorld.blocks[i] = fromWorld.blocks[i];
//            if(ours != null) {
//                gameWorld.blocks[i].body = ours.body;
//            }
        }

        updatePhysics();
    }

    public GameWorld getWorldSnapshot() {
        return kryo.copy(gameWorld);
    }

    public GameWorld getWorldReference() {
        return gameWorld;
    }


    public World getBox() {
        return boxWorld;
    }
}
