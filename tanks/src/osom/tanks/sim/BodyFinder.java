package osom.tanks.sim;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.World;
import osom.tanks.entities.BaseBlock;

import java.util.HashMap;

/**
 * Connects GameWorld entities with they Box2D counterparts.
 */
public class BodyFinder {
    private Body[] boxBodies = new Body[Configuration.MAX_BLOCKS];
    private HashMap<String, Joint> jointHashMap = new HashMap<String, Joint>();

    public Body getBody(BaseBlock block) {
        return boxBodies[block.id];
    }

    public Body setBody(BaseBlock block, Body body) {
        boxBodies[block.id] = body;
        return body;
    }

    public void updateJoint(BaseBlock block1, BaseBlock block2, World world) {
        if(block2.id < block1.id) {
            BaseBlock swap = block1;
            block1 = block2;
            block2 = swap;
        }

        Joint joint = jointHashMap.get("" + block1.id + "-" + block2.id);
        if(joint == null) {
            joint = BaseBlock.createBlockJoint(getBody(block1), getBody(block2), world);
            jointHashMap.put("" + block1.id + "-" + block2.id, joint);
        }
    }
}
