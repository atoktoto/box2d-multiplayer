package osom.tanks.sim;

import com.esotericsoftware.minlog.Log;
import osom.tanks.entities.Player;
import osom.tanks.messages.GameMessage;
import osom.tanks.messages.WorldUpdate;

public class ClientSimulationRunner extends SimulationRunner {
    public Player self;

    public ClientSimulationRunner(GameWorld gameWorld) {
        super(gameWorld);
    }

    public void updateStateWith(WorldUpdate update) {
        GameWorld fromWorld = update.gameWorld;

        long stateAge = gameWorld.frameCnt - fromWorld.frameCnt;
        Log.error("State age: " + stateAge);

        if(update.syncFrame) {
            gameWorld.frameCnt = fromWorld.frameCnt;
            Log.error("Frame sync, state age: " + stateAge);
            stateAge = 0;
        }

        if(stateAge == 0) {         // local update
            setWorldState(fromWorld);
        } else if(stateAge > 0) {   // old update (local actions are ahead)
            //TODO apply old not-acknowledged actions
            setWorldState(fromWorld);
            advanceSimulation(stateAge);
        } else {
            //message from future!
            setWorldState(fromWorld); // just apply
        }
    }

    public void runLocalAction(GameMessage click, Player who) {
        gameWorld.executeMessage(click, who);
        updatePhysics();
    }

    public void setPlayer(Player player) {
        this.self = player;
    }
}
