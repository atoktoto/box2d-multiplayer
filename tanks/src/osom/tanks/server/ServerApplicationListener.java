package osom.tanks.server;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import osom.tanks.gfx.GameRenderer;
import osom.tanks.networking.ServerNetworking;
import osom.tanks.sim.GameWorld;
import osom.tanks.sim.ServerSimulationRunner;
import osom.tanks.sim.SimulationRunner;

public class ServerApplicationListener implements ApplicationListener {

    private GameRenderer gameRenderer;
    private ServerNetworking serverNetworking;
    private SimulationRunner simulationRunner;
    public final ServerConfig serverConfig;

    public ServerApplicationListener(ServerConfig serverConfig) {
        this.serverConfig = serverConfig;
    }

    @Override
    public void create() {
        GameWorld gameWorld = new GameWorld();
        gameWorld.createNew();

        serverNetworking = new ServerNetworking(serverConfig.simulateLag);
        simulationRunner = new ServerSimulationRunner(gameWorld, serverNetworking);

        if(!serverConfig.headless) {
            Gdx.graphics.setTitle("Server");
            gameRenderer = new GameRenderer(gameWorld, simulationRunner.getBox());
            gameRenderer.create();
        }

        serverNetworking.setSimulationRunner((ServerSimulationRunner) simulationRunner);
        serverNetworking.listen(serverConfig.tcp_port, serverConfig.udp_port);
    }

    @Override
    public void render() {
        serverNetworking.runQueue();
        if(!serverConfig.headless) {
            gameRenderer.render();
        }
        simulationRunner.tick();
    }

    @Override
    public void resize(int width, int height) {
        if(gameRenderer != null)
            gameRenderer.resize(width, height);
    }

    @Override
    public void pause() {
        if(gameRenderer != null)
            gameRenderer.pause();
    }

    @Override
    public void resume() {
        if(gameRenderer != null)
            gameRenderer.resume();
    }

    @Override
    public void dispose() {
        if(gameRenderer != null)
            gameRenderer.dispose();
    }
}
