package osom.tanks.server;

/**
 * Container used during server startup.
 */
public class ServerConfig {
    public int tcp_port = 54555;
    public int udp_port = 54777;
    public boolean simulateLag = false;
    public boolean headless = false;
}
