package osom.tanks.networking;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import osom.tanks.entities.Player;
import osom.tanks.sim.ServerSimulationRunner;
import osom.tanks.messages.GameMessage;
import osom.tanks.messages.WorldUpdate;

import java.io.IOException;
import java.util.HashMap;

public class ServerNetworking {

    private Server server;
    private MyQueuedListener networkListener;
    private ServerSimulationRunner simulationRunner;
    private HashMap<Integer, Player> playerMap = new HashMap<Integer, Player>();
    private boolean simulateLag = false;

    public ServerNetworking(boolean simulateLag) {
        this.simulateLag = simulateLag;
        server = new Server();
        KryoProvider.setupKryo(server.getKryo());
    }

    public void setSimulationRunner(ServerSimulationRunner runner) {
        this.simulationRunner = runner;
    }

    public void listen(int tcp_port, int udp_port) {
        server = new Server();
        KryoProvider.setupKryo(server.getKryo());

        server.start();
        try {
            server.bind(tcp_port, udp_port);
            if(simulateLag) {
                networkListener = new MyQueuedListener(serverListener);
                server.addListener(new Listener.LagListener(20, 300, networkListener));
            } else {
                networkListener = new MyQueuedListener(serverListener);
                server.addListener(networkListener);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runQueue() {
        networkListener.executeQueued();
    }

    Listener serverListener = new Listener() {
        @Override
        public void connected(Connection connection) {
        }

        @Override
        public void disconnected(Connection connection) {
            simulationRunner.userQuited();
        }

        @Override
        public void received(Connection connection, Object object) {
            if(object instanceof GameMessage) {
                Player player = playerMap.get(connection.getID());
                simulationRunner.executeMessage((GameMessage)object, player);
            } else if(object instanceof Player) {
                Player negotiatedPlayer = simulationRunner.userJoined((Player)object);
                playerMap.put(connection.getID(), negotiatedPlayer);
                connection.sendTCP(negotiatedPlayer);
            }
        }

        @Override
        public void idle(Connection connection) {
            super.idle(connection);
        }
    };

    public void sendWorldUpdate(WorldUpdate worldUpdate) {
        server.sendToAllUDP(worldUpdate);
    }

}
