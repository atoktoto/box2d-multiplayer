package osom.tanks.networking;

import com.esotericsoftware.kryo.Kryo;
import osom.tanks.entities.BaseBlock;
import osom.tanks.entities.EngineBlock;
import osom.tanks.entities.Player;
import osom.tanks.sim.GameWorld;
import osom.tanks.messages.EngineStateChange;
import osom.tanks.messages.TestClickAction;
import osom.tanks.messages.WorldUpdate;

import java.util.HashMap;

public class KryoProvider {

    public static void setupKryo(Kryo kryo) {

        kryo.setReferences(false);

        kryo.register(TestClickAction.class);
        kryo.register(WorldUpdate.class);
        kryo.register(GameWorld.class);
        kryo.register(BaseBlock.class);
        kryo.register(BaseBlock[].class);
        kryo.register(EngineBlock.class);
        kryo.register(HashMap.class);
        kryo.register(EngineStateChange.class);
        kryo.register(Player.class);
        kryo.register(int[].class);
    }


}
