package osom.tanks.networking;


import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;
import osom.tanks.entities.Player;
import osom.tanks.messages.GameMessage;
import osom.tanks.messages.WorldUpdate;
import osom.tanks.sim.ClientSimulationRunner;

import java.io.IOException;

public class ClientNetworking {

    private Client client;
    private MyQueuedListener networkListener;
    private ClientSimulationRunner simulationRunner;
    private boolean simulateLag = false;

    public ClientNetworking(boolean simulateLag) {
        this.simulateLag = simulateLag;
        client = new Client();
        KryoProvider.setupKryo(client.getKryo());
    }

    public void setSimulationRunner(ClientSimulationRunner simulationRunner) {
        this.simulationRunner = simulationRunner;
    }

    public void connect(String address) throws IOException {
        client.start();

        if(simulateLag) {
            networkListener = new MyQueuedListener(clientListener);
            client.addListener(new Listener.LagListener(20, 300, networkListener));
        } else {
            networkListener = new MyQueuedListener(clientListener);
            client.addListener(networkListener);
        }
        client.connect(5000, address, 54555, 54777);
    }

    public void runQueue() {
        networkListener.executeQueued();
    }

    Listener clientListener = new Listener() {
        @Override
        public void connected(Connection connection) {
            super.connected(connection);
            client.sendTCP(new Player());
        }

        @Override
        public void disconnected(Connection connection) {
            super.disconnected(connection);
        }

        @Override
        public void received(Connection connection, Object object) {
            if(object instanceof WorldUpdate) {
                simulationRunner.updateStateWith(((WorldUpdate) object));
            } else if(object instanceof Player) {
                simulationRunner.setPlayer((Player)object);
            }
        }

        @Override
        public void idle(Connection connection) {
            super.idle(connection);
        }
    };

    public void sendMessage(GameMessage gameMessage) {
        client.sendUDP(gameMessage);
    }
}
