package osom.tanks.networking;

import com.esotericsoftware.kryonet.Listener;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Used to transfer messages from network threads back to GLThread (gdx)
 */
public class MyQueuedListener extends Listener.QueuedListener {

    private final ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<Runnable>();

    public MyQueuedListener(Listener listener) {
        super(listener);
    }

    @Override
    protected void queue(Runnable runnable) {
        queue.add(runnable);
    }

    /**
     * Executes queued runnables on calling thread. Should be called on GLThread.
     */
    public void executeQueued() {
        while(true) {
            Runnable runnable = queue.poll();
            if(runnable == null)
                break;
            runnable.run();
        }
    }

}
