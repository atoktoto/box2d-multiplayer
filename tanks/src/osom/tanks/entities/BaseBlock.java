package osom.tanks.entities;

import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import osom.tanks.sim.BodyFinder;
import osom.tanks.sim.GameWorld;

public class BaseBlock implements PhysicalBody {

    public float positionX, positionY;
    public float linearVelocityX, linearVelocityY;

    private float angularVelocity;
    private float angle;

    public int id = -1;
    public int[] links = {-1, -1, -1, -1};

    public void updateJoints(Body body, World world, GameWorld gameWorld, BodyFinder finder) {
        for(int link : links) {
            if(link != -1) {
                BaseBlock block = gameWorld.blocks[link];
                finder.updateJoint(this, block, world);
            }
        }
    }

    public Body updateBody(Body body, World world, GameWorld gameWorld) {
        if(body == null)
            body = BaseBlock.createBlockBody(world);

        body.setTransform(positionX, positionY, angle);
        body.setLinearVelocity(linearVelocityX, linearVelocityY);
        body.setAngularVelocity(angularVelocity);

        return body;
    }


    public void updateObject(Body body) {
        positionX = body.getTransform().getPosition().x;
        positionY = body.getTransform().getPosition().y;
        angle = body.getTransform().getRotation();

        linearVelocityX = body.getLinearVelocity().x;
        linearVelocityY = body.getLinearVelocity().y;

        angularVelocity = body.getAngularVelocity();
    }

    public void setPosition(float x, float y) {
        positionY = y;
        positionX = x;
    }

    public static Body createBlockBody(World world) {

        // First we create a body definition
        BodyDef bodyDef = new BodyDef();
        // We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        // Set our body's starting position in the world
        bodyDef.position.set(0, 0);
        bodyDef.linearDamping = 2;

        // Create our body in the world using our body definition
        Body body = world.createBody(bodyDef);

        // Create a circle shape and set its radius to 6
        PolygonShape blockShape = new PolygonShape();
        blockShape.setAsBox(1f, 1f);

        // Create a fixture definition to apply our shape to
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = blockShape;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f; // Make it bounce a little bit

        // Create our fixture and attach it to the body
        Fixture fixture = body.createFixture(fixtureDef);

        // Remember to dispose of any shapes after you're done with them!
        // BodyDef and FixtureDef don't need disposing, but shapes do.
        blockShape.dispose();

        return body;
    }

    public void tick(Body body) {}


    public static Joint createBlockJoint(Body block1, Body block2, World world) {
        PrismaticJointDef jointDef = new PrismaticJointDef();
        jointDef.bodyA = block1;
        jointDef.bodyB = block2;
        jointDef.localAnchorA.set(0,0);
        jointDef.localAnchorB.set(0,0);
        jointDef.collideConnected = true;

        jointDef.localAxisA.set(block1.getLocalVector(block2.getTransform().getPosition().cpy().sub(block1.getTransform().getPosition()).nor()));
//        jointDef.referenceAngle = block1.getAngle() - block2.getAngle();

        jointDef.lowerTranslation = 2.1f;
        jointDef.upperTranslation = 5f;
        jointDef.enableLimit = true;

        jointDef.motorSpeed = -2f;
        jointDef.maxMotorForce = 30;  //10
        jointDef.enableMotor = true;

        return world.createJoint(jointDef);

    }
}
