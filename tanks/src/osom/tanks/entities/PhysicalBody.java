package osom.tanks.entities;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import osom.tanks.sim.BodyFinder;
import osom.tanks.sim.GameWorld;

public interface PhysicalBody {
    public void updateJoints(Body body, World world, GameWorld gameWorld, BodyFinder finder);
    public Body updateBody(Body body, World world, GameWorld gameWorld);
}
