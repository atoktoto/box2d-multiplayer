package osom.tanks.entities;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.esotericsoftware.minlog.Log;

public class EngineBlock extends BaseBlock {

    private int thrustAngle = 0;
    private float thrustPower = 1;
    public boolean thrustOn = false;
    public int owner;
    public int engineGroup = -1;

    public void setThrustAngle(int thrustAngle) {
        this.thrustAngle = thrustAngle;
    }
    public void setThrustPower(float thrustPower) {
        this.thrustPower = thrustPower;
    }
    public void setThrustOn(boolean thrustOn) {
        this.thrustOn = thrustOn;
    }


    private transient Vector2 impulse = new Vector2();

    @Override
    public void tick(Body body) {
        super.tick(body);

        if(thrustOn) {
            float bodyRotation = body.getAngle() * MathUtils.radDeg;
            impulse.set(thrustPower, 0).rotate(thrustAngle + bodyRotation);
            body.applyLinearImpulse(impulse, body.getPosition(), true);
        }
    }
}
