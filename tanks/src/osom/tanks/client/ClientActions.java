package osom.tanks.client;

import com.esotericsoftware.minlog.Log;
import osom.tanks.gfx.GameRenderer;
import osom.tanks.sim.ClientSimulationRunner;
import osom.tanks.sim.GameWorld;
import osom.tanks.messages.EngineStateChange;
import osom.tanks.messages.TestClickAction;
import osom.tanks.networking.ClientNetworking;
import osom.tanks.sim.SimulationRunner;

/**
 * Place for all the actions that are run locally as a result of user input.
 */
public class ClientActions {

    private final ClientNetworking client;
    private final GameWorld gameWorld;
    private final GameRenderer gameRenderer;
    private final ClientSimulationRunner simulationRunner;

    public ClientActions(ClientNetworking client, GameWorld gameWorld, GameRenderer gameRenderer, ClientSimulationRunner simulationRunner) {
        this.client = client;
        this.gameWorld = gameWorld;
        this.gameRenderer = gameRenderer;
        this.simulationRunner = simulationRunner;
    }

    public void engineOn(int engineGroup) {
        EngineStateChange engineStateChange = new EngineStateChange();
        engineStateChange.on = true;
        engineStateChange.engineGroup = engineGroup;
        engineStateChange.frameCnt = gameWorld.frameCnt;
        client.sendMessage(engineStateChange);

        simulationRunner.runLocalAction(engineStateChange, simulationRunner.self);
    }

    public void engineOff(int engineGroup) {
        EngineStateChange engineStateChange = new EngineStateChange();
        engineStateChange.on = false;
        engineStateChange.engineGroup = engineGroup;
        engineStateChange.frameCnt = gameWorld.frameCnt;
        client.sendMessage(engineStateChange);

        simulationRunner.runLocalAction(engineStateChange, simulationRunner.self);
    }

    public void testClick(float x, float y) {
        TestClickAction click = new TestClickAction();
        click.x = x;
        click.y = y;
        click.frameCnt = gameWorld.frameCnt;

        client.sendMessage(click);
        simulationRunner.runLocalAction(click, simulationRunner.self);
    }

    public void setCameraZoom(float amount) {
        gameRenderer.requestZoom(amount);
    }

    public void cameraTranslate(float x, float y) {
        gameRenderer.cameraTranslate(x, y);
    }
}
