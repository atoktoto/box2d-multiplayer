package osom.tanks.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import osom.tanks.client.ClientActions;

public class GameInputListener extends InputListener {

    private float lastDragX = 0, lastDragY = 0;
    private ClientActions clientActions;

    public GameInputListener(ClientActions clientActions) {
        this.clientActions = clientActions;
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        if(keycode == Input.Keys.W) {
            clientActions.engineOn(1);
        }
        if(keycode == Input.Keys.A) {
            clientActions.engineOn(2);
        }
        if(keycode == Input.Keys.S) {
            clientActions.engineOn(3);
        }
        if(keycode == Input.Keys.D) {
            clientActions.engineOn(4);
        }
        return super.keyDown(event, keycode);
    }

    @Override
    public boolean keyUp(InputEvent event, int keycode) {
        if(keycode == Input.Keys.W) {
            clientActions.engineOff(1);
        }
        if(keycode == Input.Keys.A) {
            clientActions.engineOff(2);
        }
        if(keycode == Input.Keys.S) {
            clientActions.engineOff(3);
        }
        if(keycode == Input.Keys.D) {
            clientActions.engineOff(4);
        }
        return super.keyUp(event, keycode);
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.LEFT) {
            clientActions.testClick(x, y);
        }

        if (button == Input.Buttons.MIDDLE) {
            lastDragX = x;
            lastDragY = y;
            return true;
        }

        return false;
    }

    @Override
    public boolean scrolled(InputEvent event, float x, float y, int amount) {
        clientActions.setCameraZoom(0.07f * (float) amount);
        return true;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (Gdx.input.isButtonPressed(Input.Buttons.MIDDLE)) {
            clientActions.cameraTranslate(lastDragX - x,lastDragY - y);
        }

        super.touchDragged(event, x, y, pointer);
    }
}
