package osom.tanks.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.minlog.Log;
import osom.tanks.gfx.GameRenderer;
import osom.tanks.networking.ClientNetworking;
import osom.tanks.sim.ClientSimulationRunner;
import osom.tanks.sim.Configuration;
import osom.tanks.sim.GameWorld;
import osom.tanks.sim.SimulationRunner;

import java.io.IOException;

public class ClientApplicationListener implements ApplicationListener {

    private GameRenderer gameRenderer;

    private ClientNetworking clientNetworking;
    private SimulationRunner simulationRunner;
    public final ClientConfig clientConfig;

    public ClientApplicationListener(ClientConfig clientConfig) {
        this.clientConfig = clientConfig;
    }




    @Override
	public void create() {

        GameWorld gameWorld = new GameWorld();
        gameWorld.createNew();

        clientNetworking = new ClientNetworking(clientConfig.simulateLag);
        simulationRunner = new ClientSimulationRunner(gameWorld);

        gameRenderer = new GameRenderer(gameWorld, simulationRunner.getBox());
        gameRenderer.create();

        clientNetworking.setSimulationRunner((ClientSimulationRunner) simulationRunner);
        try {
            clientNetworking.connect(clientConfig.address);
        } catch (IOException e) {
            Log.error(e.getMessage());
            Gdx.app.exit();
        }

        ClientActions clientActions = new ClientActions(clientNetworking, gameWorld, gameRenderer, (ClientSimulationRunner) simulationRunner);
        GameInputListener inputListener = new GameInputListener(clientActions);
        gameRenderer.setInputListener(inputListener);
    }

	@Override
	public void dispose() {
        gameRenderer.dispose();
	}

    long nextFrameTime = 0;

    int avgCnt = 0;
    long frameLenBuff[] = new long[100];
    long lastFrame = 0;

	@Override
	public void render() {
        clientNetworking.runQueue();
        gameRenderer.render();

        if(nextFrameTime == 0) {
            nextFrameTime = System.currentTimeMillis() - 1;
        }

        if(System.currentTimeMillis() > nextFrameTime) {

            nextFrameTime = nextFrameTime + Configuration.SIM_FRAME_TIME;
            simulationRunner.tick();

            long len = System.currentTimeMillis() - lastFrame;
            frameLenBuff[avgCnt] = len;
            avgCnt++;
            avgCnt = avgCnt % frameLenBuff.length;

            float sum = 0;
            for(long l : frameLenBuff) {
                sum += l;
            }

//            Log.error("Avrg frame time: " + sum / (float)frameLenBuff.length);
            lastFrame = System.currentTimeMillis();
        }
	}

	@Override
	public void resize(int width, int height) {
        gameRenderer.resize(width, height);
	}

	@Override
	public void pause() {
        gameRenderer.pause();
	}

	@Override
	public void resume() {
        gameRenderer.resume();
	}
}
